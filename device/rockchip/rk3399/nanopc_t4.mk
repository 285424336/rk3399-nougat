#
# Copyright 2014 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRIV_DEVICE_PATH := device/rockchip/rk3399/nanopc-t4

# Quectel
$(call inherit-product-if-exists, vendor/quectel/ec20/device-partial.mk)

# Inherit from those products. Most specific first.
$(call inherit-product, $(LOCAL_PATH)/rk3399.mk)

PRODUCT_NAME := nanopc_t4
PRODUCT_DEVICE := nanopc-t4
PRODUCT_BRAND := Android
PRODUCT_MODEL := NanoPC-T4 (RK3399)
PRODUCT_MANUFACTURER := FriendlyARM (www.friendlyarm.com)

# Screen size is "normal", density is "hdpi"
PRODUCT_AAPT_CONFIG := normal large xlarge mdpi hdpi xhdpi
PRODUCT_AAPT_PREF_CONFIG := hdpi

# These are the hardware-specific features
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
    frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml \
    frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
    frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
    frameworks/native/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml \
    frameworks/native/data/etc/android.hardware.audio.pro.xml:system/etc/permissions/android.hardware.audio.pro.xml \
    frameworks/native/data/etc/android.hardware.telephony.cdma.xml:system/etc/permissions/android.hardware.telephony.cdma.xml \
    frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
    frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml

# Camera
BOARD_CAMERA_XML_CONFIG := device/rockchip/rk3399/nanopc-t4/camera/cam_board_A.xml
PRODUCT_COPY_FILES += \
    device/rockchip/rk3399/nanopc-t4/camera/OV13850_lens_50013A1.xml:system/etc/OV13850_lens_50013A1.xml

# vendor apps
$(call inherit-product-if-exists, vendor/friendlyelec/apps/device-partial.mk)

# google gms
$(call inherit-product-if-exists, vendor/google/gapps/device-partial.mk)

# debug-logs
ifneq ($(TARGET_BUILD_VARIANT),user)
MIXIN_DEBUG_LOGS ?= true
endif

BUILD_WITH_LIGHTNING := false

# for drm widevine
BUILD_WITH_WIDEVINE := true

BOARD_USB_HOST_SUPPORT := true
BOARD_USE_APP_ALARM_ALIGNMENT ?= true

PRODUCT_COPY_FILES += \
    $(PRIV_DEVICE_PATH)/ddr_config.xml:system/etc/ddr_config.xml \
    $(PRIV_DEVICE_PATH)/video_status:system/etc/video_status

# Add product overlay
ifeq ($(TARGET_DEVICE_DIR),)
PRODUCT_PACKAGE_OVERLAYS += $(PRIV_DEVICE_PATH)/overlay
endif

